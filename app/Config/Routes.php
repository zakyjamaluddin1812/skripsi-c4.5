<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', function() {
    if(session()->get('login') == true) {
        return redirect()->to(base_url('/dashboard'));
    }
    return view('sign-in');
});
$routes->get('/daftar', function() {
    if(session()->get('login') == true) {
        return redirect()->to(base_url('/dashboard'));
    }
    return view('sign-up');
});
$routes->get('/logout', 'Auth::logout');
$routes->get('/dashboard', 'Dashboard::index');
$routes->get('/tables', 'Penjualan::index');    
$routes->get('/detail-barang/(:num)', 'Penjualan::detailBarang/$1');
$routes->get('/detail-penjualan/(:num)', 'Penjualan::detailPenjualan/$1');
$routes->post('/tambah-barang', 'Penjualan::tambah_barang');
$routes->post('/tambah-penjualan', 'Penjualan::tambah_penjualan');
$routes->get('/hapus-barang/(:num)', 'Penjualan::hapus_barang/$1');

$routes->get('/hapus-penjualan/(:num)/(:num)', 'Penjualan::hapus_penjualan/$1/$2');
// $routes->get('/hapus-penjualan/(:num)', function($id) {
//     dd($id);
// });

$routes->get('/decission-detail', 'Decission::detail');
$routes->get('/decission', 'Decission::index');
$routes->get('/ramalan', 'Decission::ramalan');
$routes->post('/ramal', 'Decission::ramal');

$routes->post('/daftar', 'Auth::daftar');
$routes->post('/login', 'Auth::login');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
