<?php

namespace App\Models;

use CodeIgniter\Model;

class Barang extends Model
{
    protected $table      = 'barang';
    protected $allowedFields  = ['nama', 'kategori', 'satuan', 'ukuran', 'model', 'kualitas_bahan', 'warna', 'penjualan', 'minat'];

}