<?php

namespace App\Models;

use CodeIgniter\Model;

class Decission extends Model
{
    protected $attribute;
    protected $barang;
    protected $total_semua;
    protected $total_node1;
    protected $total_node2;
    protected $total_node3;
    protected $total_node4;
    protected $entrophy_semua;
    protected $entrophy_node1;
    protected $entrophy_node2;
    protected $entrophy_node3;
    protected $entrophy_node4;
    protected $index_node1;
    protected $index_node2;
    protected $index_node3;
    protected $index_node4;
    public function __construct() {
        $this->index_node1 = [];
        $this->index_node2 = [];
        $this->index_node3 = [];
        $this->index_node4 = [];
        $this->total_semua = [];
        $this->total_node1 = [];
        $this->total_node2 = [];
        $this->total_node3 = [];
        $this->total_node4 = [];
        $this->entrophy_semua = [];
        $this->entrophy_node1 = [];
        $this->entrophy_node2 = [];
        $this->entrophy_node3 = [];
        $this->entrophy_node4 = [];
        $this->barang = model('Barang');
        $this->attribute = [
            ['ukuran' => [
                'allsize',
                'oversize'
            ]],
            ['model' => [
                'Atasan',
                'Bawahan'
            ]],
            ['kualitas_bahan' => [
                'baik',
                'premium'
            ]],
            ['warna' => [
                'banyak',
                'sedikit'
            ]]
        ];
    }
    public function pulihkan() {
        $this->attribute = [
            ['ukuran' => [
                'allsize',
                'oversize'
            ]],
            ['model' => [
                'Atasan',
                'Bawahan'
            ]],
            ['kualitas_bahan' => [
                'baik',
                'premium'
            ]],
            ['warna' => [
                'banyak',
                'sedikit'
            ]]
        ];
    }
    public function decission ($model, $warna, $ukuran, $kualitas_bahan) {

        $minat;
        if($ukuran == "allsize") {
            if ($warna == "banyak") {
                if ($model == "Atasan") {
                    if ($kualitas_bahan == "baik") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                } else {
                    if ($kualitas_bahan == "baik") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                }
            } else {
                if ($model == "Atasan") {
                    if ($kualitas_bahan == "baik") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                } else {
                    if ($kualitas_bahan == "baik") {
                        $minat = "turun";
                    } else {
                        $minat = "turun";
                    }
                }
            }
        } else {
            if ($kualitas_bahan == "baik") {
                if ($model == "Atasan") {
                    if ($warna == "banyak") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                } else {
                    if ($warna == "banyak") {
                        $minat = "turun";
                    } else {
                        $minat = "turun";
                    }
                }
            } else {
                if ($warna == "banyak") {
                    if ($model == "Atasan") {
                        $minat = "turun";
                    } else {
                        $minat = "turun";
                    }
                } else {
                    if ($model == "Atasan") {
                        $minat = "naik";
                    } else {
                        $minat = "turun";
                    }
                }
            }
        }

        return $minat;
    }


    public function semua() {
        $total = count($this->barang->findAll());
        $ya = count($this->barang->where('minat', 'naik')->findAll());
        $tidak = count($this->barang->where('minat', 'turun')->findAll());
        $entrophy = $this->getEntrophy($total, $ya, $tidak);
        $semua = [
            "total" => $total,
            "ya" => $ya,
            "tidak" => $tidak,
            "entrophy" => $entrophy
        ];
        array_push($this->total_semua, $total);
        array_push($this->entrophy_semua, $entrophy);
        return $semua;
    }

    public function node1() {
        $node1 = [];
        foreach ($this->attribute as $key => $attribute) {
            $node = [];
            $key = array_keys($attribute)[0];
            foreach ($attribute[$key] as $value) {
                $total = count($this->barang->where($key, $value)->findAll());
                $ya = count($this->barang->where($key, $value)->where('minat', 'naik')->findAll());
                $tidak = count($this->barang->where($key, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($total, $ya, $tidak);
                $semua = [
                    "key" => $key,
                    "value" => $value,
                    "total" => $total,
                    "ya" => $ya,
                    "tidak" => $tidak,
                    "entrophy" => $entrophy
                ];
                array_push($node, $semua);
            }
            $gain = $this->getGain(
                $this->entrophy_semua[0], 
                $this->total_semua[0], 
                $node[0]['entrophy'], 
                $node[0]['total'],
                $node[1]['entrophy'], 
                $node[1]['total']
            );
            $gain_array = [
                "gain" => $gain
            ];
            $value1 = array_merge($node[0], $gain_array);
            $value2 = array_merge($node[1], $gain_array);
            array_push($node1, $value1);
            array_push($node1, $value2);
        }

        $gains = [];
        foreach ($node1 as $key => $value) {
            array_push($gains, $value['gain']);
        }
        $max = max($gains);
        $this->index_node1 = array_search($max, $gains);
        $node1_final = [];
        foreach ($node1 as $key => $value) {
            if($value['gain'] == $max) {
                $max_array = [
                    "max" => true
                ];
                $node1_test = array_merge($value, $max_array);
                array_push($node1_final, $node1_test);   
                // array_push($this->index_node1, $value['key']);
            } else {
                $max_array = [
                    "max" => false
                ];
                $node1_test = array_merge($value, $max_array); 
                array_push($node1_final, $node1_test);         

            }
        }
        return $node1_final;
    }
    public function node2() {
        $attribute = $this->attribute[$this->index_node1];
        unset($this->attribute[$this->index_node1]);
        $k1 = array_keys($attribute)[0];
        $node2_final = [];
        foreach ($attribute[$k1] as $v1) {
            $node2WithMax = [];
            $node2 = [];
            $totalSemua = count($this->barang->where($k1, $v1)->findAll());
            $yaSemua = count($this->barang->where($k1, $v1)->where('minat', 'naik')->findAll());
            $tidakSemua = count($this->barang->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophySemua = $this->getEntrophy($totalSemua, $yaSemua, $tidakSemua);
            foreach($this->attribute as $attribute_node2) {
                $k2 = array_keys($attribute_node2)[0];
                $node = [];
                foreach($attribute_node2[$k2] as $v2) {
                    $total = count($this->barang->where($k1, $v1)->where($k2, $v2)->findAll());
                    $ya = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                    $tidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                    $entrophy = $this->getEntrophy($total, $ya, $tidak);
                    $semua = [
                        "key" => $k2,
                        "value" => $v2,
                        "total" => $total,
                        "ya" => $ya,
                        "tidak" => $tidak,
                        "entrophy" => $entrophy
                    ];
                    array_push($node, $semua);
                }
                $gain = $this->getGain(
                    $entrophySemua, 
                    $totalSemua, 
                    $node[0]['entrophy'], 
                    $node[0]['total'],
                    $node[1]['entrophy'], 
                    $node[1]['total']
                );
                $gain_array = [
                    "gain" => $gain
                ];
                $value1 = array_merge($node[0], $gain_array);
                $value2 = array_merge($node[1], $gain_array);
                array_push($node2, $value1);
                array_push($node2, $value2);
            }
            $gains = [];
            foreach ($node2 as $key => $value) {
                array_push($gains, $value['gain']);
            }
            $max = max($gains);
            foreach ($node2 as $key => $value) {
                if($value['gain'] == $max) {
                    $max_array = [
                        "max" => true
                    ];
                    $node1_test = array_merge($value, $max_array);
                    array_push($node2WithMax, $node1_test);   
                    // array_push($this->index_node1, $value['key']);
                } else {
                    $max_array = [
                        "max" => false
                    ];
                    $node1_test = array_merge($value, $max_array); 
                    array_push($node2WithMax, $node1_test);         

                }
            }
            array_push($node2_final, $node2WithMax);


        }
        return $node2_final;
    }
    public function node3() {
        $this->pulihkan();
        $attribute = $this->attribute[$this->index_node1];
        unset($this->attribute[$this->index_node1]);
        $k1 = array_keys($attribute)[0];
        $node3_final = [];
        foreach ($attribute[$k1] as $v1) {
            $node3WithMax = [];
            $node2 = [];
            $totalSemua = count($this->barang->where($k1, $v1)->findAll());
            $yaSemua = count($this->barang->where($k1, $v1)->where('minat', 'naik')->findAll());
            $tidakSemua = count($this->barang->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophySemua = $this->getEntrophy($totalSemua, $yaSemua, $tidakSemua);
            foreach($this->attribute as $attribute_node2) {
                $k2 = array_keys($attribute_node2)[0];
                $node = [];
                foreach($attribute_node2[$k2] as $v2) {
                    $total = count($this->barang->where($k1, $v1)->where($k2, $v2)->findAll());
                    $ya = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                    $tidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                    $entrophy = $this->getEntrophy($total, $ya, $tidak);
                    $semua = [
                        "key" => $k2,
                        "value" => $v2,
                        "total" => $total,
                        "ya" => $ya,
                        "tidak" => $tidak,
                        "entrophy" => $entrophy
                    ];
                    array_push($node, $semua);
                }
                $gain = $this->getGain(
                    $entrophySemua, 
                    $totalSemua, 
                    $node[0]['entrophy'], 
                    $node[0]['total'],
                    $node[1]['entrophy'], 
                    $node[1]['total']
                );
                $gain_array = [
                    "gain" => $gain
                ];
                $value1 = array_merge($node[0], $gain_array);
                $value2 = array_merge($node[1], $gain_array);
                array_push($node2, $value1);
                array_push($node2, $value2);
            }
            $gains = [];
            foreach ($node2 as $key => $value) {
                array_push($gains, $value['gain']);
            }
            $max = max($gains);
            $node1_final = [];
            $value_max = [];
            $key_max = [];
            foreach ($node2 as $key => $value) {
                if($value['gain'] == $max) {
                    array_push($value_max, $value['value']);
                    array_push($key_max, $value['key']);
                } 
            }
            $attribute_max_node2 = [
                $key_max[0] => $value_max
            ];

            $index_node2 = array_search($attribute_max_node2, $this->attribute);







            // Masuk Node 3
            $k2 = array_keys($attribute_max_node2)[0];
            foreach($attribute_max_node2[$k2] as $v2) {
                $node2 = [];
                $node2_with_max = [];
                $totalSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->findAll());
                $yaSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                $tidakSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                $entrophySemua = $this->getEntrophy($total, $ya, $tidak);

                $this->pulihkan();

                unset($this->attribute[$this->index_node1]);
                unset($this->attribute[$index_node2]);
                foreach($this->attribute as $attribute_node3) {
                    $k3 = array_keys($attribute_node3)[0];
                    $node = [];
                    // dd($k3, $attribute_node3);
                    foreach($attribute_node3[$k3] as $v3) {
                        $total = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->findAll());
                        $ya = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'naik')->findAll());
                        $tidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'turun')->findAll());
                        $entrophy = $this->getEntrophy($total, $ya, $tidak);
                        $semua = [
                            "key" => $k3,
                            "value" => $v3,
                            "total" => $total,
                            "ya" => $ya,
                            "tidak" => $tidak,
                            "entrophy" => $entrophy
                        ];
                        array_push($node, $semua);
                    }
                    $gain = $this->getGain(
                        $entrophySemua, 
                        $totalSemua, 
                        $node[0]['entrophy'], 
                        $node[0]['total'],
                        $node[1]['entrophy'], 
                        $node[1]['total']
                    );
                    $gain_array = [
                        "gain" => $gain
                    ];
                    $value1 = array_merge($node[0], $gain_array);
                    $value2 = array_merge($node[1], $gain_array);
                    array_push($node2, $value1);
                    array_push($node2, $value2);
                }
                $gains = [];
                foreach ($node2 as $key => $value) {
                    array_push($gains, $value['gain']);
                }
                $max = max($gains);
                $node1_final = [];
                foreach ($node2 as $key => $value) {
                    if($value['gain'] == $max) {
                        $max_array = [
                            "max" => true
                        ];
                        $node1_test = array_merge($value, $max_array);
                        array_push($node2_with_max, $node1_test);   
                        // array_push($this->index_node1, $value['key']);
                    } else {
                        $max_array = [
                            "max" => false
                        ];
                        $node1_test = array_merge($value, $max_array); 
                        array_push($node2_with_max, $node1_test);         

                    }
                    
                }
                array_push($node3_final, $node2_with_max);
                // dd($node2_with_max);
            }
            
            

            // dd($attribute_max_node2);



        }
        return $node3_final;
    }
    public function node4() {
        $this->pulihkan();
        $attribute = $this->attribute[$this->index_node1];
        unset($this->attribute[$this->index_node1]);
        $k1 = array_keys($attribute)[0];
        $node4_final = [];

        foreach ($attribute[$k1] as $v1) {
            $node3WithMax = [];
            $node2 = [];
            $totalSemua = count($this->barang->where($k1, $v1)->findAll());
            $yaSemua = count($this->barang->where($k1, $v1)->where('minat', 'naik')->findAll());
            $tidakSemua = count($this->barang->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophySemua = $this->getEntrophy($totalSemua, $yaSemua, $tidakSemua);
            foreach($this->attribute as $attribute_node2) {
                $k2 = array_keys($attribute_node2)[0];
                $node = [];
                foreach($attribute_node2[$k2] as $v2) {
                    $total = count($this->barang->where($k1, $v1)->where($k2, $v2)->findAll());
                    $ya = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                    $tidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                    $entrophy = $this->getEntrophy($total, $ya, $tidak);
                    $semua = [
                        "key" => $k2,
                        "value" => $v2,
                        "total" => $total,
                        "ya" => $ya,
                        "tidak" => $tidak,
                        "entrophy" => $entrophy
                    ];
                    array_push($node, $semua);
                }
                $gain = $this->getGain(
                    $entrophySemua, 
                    $totalSemua, 
                    $node[0]['entrophy'], 
                    $node[0]['total'],
                    $node[1]['entrophy'], 
                    $node[1]['total']
                );
                $gain_array = [
                    "gain" => $gain
                ];
                $value1 = array_merge($node[0], $gain_array);
                $value2 = array_merge($node[1], $gain_array);
                array_push($node2, $value1);
                array_push($node2, $value2);
            }
            $gains = [];
            foreach ($node2 as $key => $value) {
                array_push($gains, $value['gain']);
            }
            $max = max($gains);
            $node1_final = [];
            $value_max = [];
            $key_max = [];
            foreach ($node2 as $key => $value) {
                if($value['gain'] == $max) {
                    array_push($value_max, $value['value']);
                    array_push($key_max, $value['key']);
                } 
            }
            $attribute_max_node2 = [
                $key_max[0] => $value_max
            ];

            $index_node2 = array_search($attribute_max_node2, $this->attribute);







            // Masuk Node 3
            $k2 = array_keys($attribute_max_node2)[0];
            foreach($attribute_max_node2[$k2] as $v2) {
                $node2 = [];
                $node2_with_max = [];
                $totalSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->findAll());
                $yaSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                $tidakSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                $entrophySemua = $this->getEntrophy($total, $ya, $tidak);

                $this->pulihkan();

                unset($this->attribute[$this->index_node1]);
                unset($this->attribute[$index_node2]);
                foreach($this->attribute as $attribute_node3) {
                    $k3 = array_keys($attribute_node3)[0];
                    $node = [];
                    // dd($k3, $attribute_node3);
                    foreach($attribute_node3[$k3] as $v3) {
                        $total = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->findAll());
                        $ya = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'naik')->findAll());
                        $tidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'turun')->findAll());
                        $entrophy = $this->getEntrophy($total, $ya, $tidak);
                        $semua = [
                            "key" => $k3,
                            "value" => $v3,
                            "total" => $total,
                            "ya" => $ya,
                            "tidak" => $tidak,
                            "entrophy" => $entrophy
                        ];
                        array_push($node, $semua);
                    }
                    $gain = $this->getGain(
                        $entrophySemua, 
                        $totalSemua, 
                        $node[0]['entrophy'], 
                        $node[0]['total'],
                        $node[1]['entrophy'], 
                        $node[1]['total']
                    );
                    $gain_array = [
                        "gain" => $gain
                    ];
                    $value1 = array_merge($node[0], $gain_array);
                    $value2 = array_merge($node[1], $gain_array);
                    array_push($node2, $value1);
                    array_push($node2, $value2);
                }
                $gains = [];
                foreach ($node2 as $key => $value) {
                    array_push($gains, $value['gain']);
                }
                $max = max($gains);
                $node1_final = [];
                $value_max = [];
                $key_max = [];
                for($i = 0; $i < count($node2); $i++) {
                    if(count($value_max) == 2) {
                        break;
                    }
                    if($node2[$i]['gain'] == $max) {
                        array_push($value_max, $node2[$i]['value']);
                        array_push($key_max, $node2[$i]['key']);
                    }

                }
                
                $attribute_max_node3 = [
                    $key_max[0] => $value_max
                ];

                $index_node3 = array_search($attribute_max_node3, $this->attribute);











                // Masuk Node 4
                $k3 = array_keys($attribute_max_node3)[0];

                foreach($attribute_max_node3[$k3] as $v3) {
                    $totalSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->findAll());
                    $yaSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'naik')->findAll());
                    $tidakSemua = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'turun')->findAll());
                    $entrophySemua = $this->getEntrophy($total, $ya, $tidak);
                    
                    $this->pulihkan();
                    
                    unset($this->attribute[$this->index_node1]);
                    unset($this->attribute[$index_node2]);
                    unset($this->attribute[$index_node3]);
                    $node4 = [];
                    $node4_with_max = [];


                    foreach($this->attribute as $attribute_node4) {
                        $k4 = array_keys($attribute_node4)[0];
                        $node = [];
                        foreach($attribute_node4[$k4] as $v4) {
                            $total = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($k4, $v4)->findAll());
                            $ya = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($k4, $v4)->where('minat', 'naik')->findAll());
                            $tidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($k4, $v4)->where('minat', 'turun')->findAll());
                            $entrophy = $this->getEntrophy($total, $ya, $tidak);
                            $semua = [
                                "key" => $k4,
                                "value" => $v4,
                                "total" => $total,
                                "ya" => $ya,
                                "tidak" => $tidak,
                                "entrophy" => $entrophy
                            ];
                            array_push($node, $semua);
                        }
                        $gain = $this->getGain(
                            $entrophySemua, 
                            $totalSemua, 
                            $node[0]['entrophy'], 
                            $node[0]['total'],
                            $node[1]['entrophy'], 
                            $node[1]['total']
                        );
                        $gain_array = [
                            "gain" => $gain
                        ];
                        $value1 = array_merge($node[0], $gain_array);
                        $value2 = array_merge($node[1], $gain_array);
                        array_push($node4, $value1);
                        array_push($node4, $value2);

                        
                    }

                    $gains = [];
                    foreach ($node4 as $key => $value) {
                        array_push($gains, $value['gain']);
                    }
                    $max = max($gains);
                    $node1_final = [];
                    foreach ($node4 as $key => $value) {
                        if($value['tidak'] == 0 && $value['ya'] == 0) {
                            $max_array = [
                                "minat" => null
                            ];
                            $node1_test = array_merge($value, $max_array);
                            array_push($node4_with_max, $node1_test); 
                        }
                        elseif($value['tidak'] == 0) {
                            $max_array = [
                                "minat" => 'naik'
                            ];
                            $node1_test = array_merge($value, $max_array);
                            array_push($node4_with_max, $node1_test);   
                        } 
                        elseif ($value['ya'] == 0) {
                            $max_array = [
                                "minat" => 'turun'
                            ];
                            $node1_test = array_merge($value, $max_array); 
                            array_push($node4_with_max, $node1_test);         

                        } else {
                            //Bug
                        }
                        
                    }
                    array_push($node4_final, $node4_with_max);
                    
                    
                }
                
                
                
                
                
            }
            
            
            
            
            
            
        }
        // dd($node4_final);
        return $node4_final;
    }

    public function getEntrophy ($jumlahBarang, $jumlahYa, $jumlahTidak) {
        if($jumlahYa == 0 || $jumlahTidak == 0) {
            return 0;
        }
        $entrophy = (-$jumlahTidak/$jumlahBarang * log($jumlahTidak/$jumlahBarang, 2)) + (-$jumlahYa/$jumlahBarang * log($jumlahYa/$jumlahBarang, 2));
        return round($entrophy, 3);

    }
    public function getGain ($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2) {       
        $attr1;
        $attr2;

        if($jumlahTotal == 0 || $entrophyAttr1 == 0 || $jumlahAttr1 == 0) {
        $attr1 = 0;
        } else {
            $attr1 = round($jumlahAttr1/$jumlahTotal*$entrophyAttr1, 3);
        }
        if($jumlahTotal == 0 || $entrophyAttr2 == 0 || $jumlahAttr2 == 0) {
            $attr2 = 0;
        } else {
            $attr2 = round($jumlahAttr2/$jumlahTotal*$entrophyAttr2, 3);
        }
        $gain = round($entrophyTotal-($attr1+$attr2), 3);
        return round($gain, 3);

    }
}