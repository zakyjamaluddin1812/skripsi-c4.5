<?php

namespace App\Models;

use CodeIgniter\Model;

class Metode extends Model
{
    protected $barang;
    protected $attribute;
    protected $metodeKedua;
    protected $node1;
    protected $node2;
    protected $node3;
    protected $node4;



    public function __construct () {
        $this->metodeKedua = model('MetodeKedua');
        $this->barang = model('Barang');
        $this->attribute = [
            ['ukuran' => [
                'allsize',
                'oversize'
            ]],
            ['model' => [
                'Atasan',
                'Bawahan'
            ]],
            ['kualitas_bahan' => [
                'baik',
                'premium'
            ]],
            ['warna' => [
                'banyak',
                'sedikit'
            ]]
        ];
    }

    public function pulihkan() {
        $this->attribute = [
            ['ukuran' => [
                'allsize',
                'oversize'
            ]],
            ['model' => [
                'Atasan',
                'Bawahan'
            ]],
            ['kualitas_bahan' => [
                'baik',
                'premium'
            ]],
            ['warna' => [
                'banyak',
                'sedikit'
            ]]
        ];
    }

    public function total() {
        $this->pulihkan();

        $jumlahBarang = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());
        $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
        $attrNode1 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);


        $total = [
            "total" => $jumlahBarang,
            "ya" => $jumlahYa,
            "tidak" => $jumlahTidak,
            "entrophy" => $entrophy
        ];
        return $total;

    }

    public function total_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());

        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($this->attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }

        return $wadahGain;


        // test
    }
    public function node1_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());

        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        $wadah_data_smt = [];     

        $attrNode1 = $this->getAttrNode1($this->attribute, $jumlahTotal, $entrophyTotal);
        // dd($attrNode1);

        $this->pulihkan();
        $nama = array_keys($attrNode1);
        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node1 = $index;
        
        // array_splice($this->attribute, $index, 1);
        unset($this->attribute[$this->node1]);

        // dd($this->attribute);



        $key = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        $wadah = [];
        foreach ($values as $value) {
            $jumlahBarang = count($this->barang->where($key, $value)->findAll());
            $jumlahYa = count($this->barang->where($key, $value)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($key, $value)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
            $attrAllNode = $this->getAttrAllNode1($this->attribute, $jumlahBarang, $entrophy, $key, $value);
            array_push($wadah, $attrAllNode);

        }


        return $wadah;
    }
    public function node2_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());

        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($this->attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadah_data_smt
            ];
            array_push($wadahGain, $data);

        }

        $attrNode1 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophyTotal);

        $this->pulihkan();
        $nama = array_keys($attrNode1);
        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node1 = $index;
        
        // array_splice($this->attribute, $index, 1);
        unset($this->attribute[$this->node1]);

        // dd($this->attribute);


        $wadah = [];

        $k1 = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        foreach ($values as $v1) {
            $jumlahBarang = count($this->barang->where($k1, $v1)->findAll());
            $jumlahYa = count($this->barang->where($k1, $v1)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);


            $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
            // $attrAllNode = $this->getAttrAllNode($this->attribute, $jumlahBarang, $entrophy);
            // array_push($wadah, $attrAllNode);

            // Pembatas
            $this->pulihkan();
            $nama = array_keys($attrNode2);

            // dd($nama);
            $namas = [];
            foreach ($this->attribute as $attr) {
                $n = array_keys($attr)[0];
                array_push($namas, $n);
            }
            $index = array_search($nama[0], $namas);
            $this->node2 = $index;

            unset($this->attribute[$this->node1], $this->attribute[$this->node2]);
            // dd($this->attribute, $this->node1, $this->node2);
            $k2 = array_keys($attrNode2)[0];
            $values = array_values($attrNode2)[0];

            
            
            
            foreach ($values as $v2) {

                // dd($k1, $v1, $k2, $v2);

                $jumlahBarang = count($this->barang->where($k2, $v2)->where($k1, $v1)->findAll());
                $jumlahYa = count($this->barang->where($k2, $v2)->where($k1, $v1)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($k2, $v2)->where($k1, $v1)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

                // $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);

                $attrAllNode = $this->getAttrAllNode2($this->attribute, $jumlahBarang, $entrophy, $k1, $v1, $k2, $v2);

                array_push($wadah, $attrAllNode);
            }
        }
        return $wadah;
    }

    public function node3_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());

        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($this->attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadah_data_smt
            ];
            array_push($wadahGain, $data);

        }


        $attrNode1 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophyTotal);

        $this->pulihkan();
        $nama = array_keys($attrNode1);
        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node1 = $index;
        
        // array_splice($this->attribute, $index, 1);
        unset($this->attribute[$this->node1]);

        // dd($this->attribute);


        $wadah = [];

        $k1 = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        foreach ($values as $v1) {
            $jumlahBarang = count($this->barang->where($k1, $v1)->findAll());
            $jumlahYa = count($this->barang->where($k1, $v1)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attrNode2 = $this->getAttrAllNode1Max($this->attribute, $jumlahBarang, $entrophy, $k1, $v1);
            // dd($attrNode2);
            // $attrAllNode = $this->getAttrAllNode($this->attribute, $jumlahBarang, $entrophy);
            // array_push($wadah, $attrAllNode);

            // Pembatas
            $this->pulihkan();
            $nama = array_keys($attrNode2);
            
            // dd($nama);
            $namas = [];
            foreach ($this->attribute as $attr) {
                $n = array_keys($attr)[0];
                array_push($namas, $n);
            }
            $index = array_search($nama[0], $namas);
            $this->node2 = $index;
            unset($this->attribute[$this->node1], $this->attribute[$this->node2]);
            // dd($this->attribute);
            $k2 = array_keys($attrNode2)[0];
            $values = array_values($attrNode2)[0];
            foreach ($values as $v2) {
                $jumlahBarang = count($this->barang->where($k2, $v2)->where($k1, $v1)->findAll());
                $jumlahYa = count($this->barang->where($k2, $v2)->where($k1, $v1)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($k2, $v2)->where($k1, $v1)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

                $attrAllNode = $this->getAttrAllNode2($this->attribute, $jumlahBarang, $entrophy, $k1, $v1, $k2, $v2);
                array_push($wadah, $attrAllNode);

                // $attrNode3 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
                // $this->pulihkan();


                // $nama = array_keys($attrNode3);
                // // dd($nama);
                // $namas = [];
                // foreach ($this->attribute as $attr) {
                //     $n = array_keys($attr)[0];
                //     array_push($namas, $n);
                // }
                // $index = array_search($nama[0], $namas);
                // // dd($nama, $namas, $index);
                // $this->node3 = $index;
                // unset($this->attribute[$this->node1], $this->attribute[$this->node2], $this->attribute[$this->node3]);
                // $k3 = array_keys($attrNode3)[0];
                // $values = array_values($attrNode3)[0];
                // foreach ($values as $v3) {
                //     $jumlahBarang = count($this->barang->where($k3, $v3)->where($k1, $v1)->where($k2, $v2)->findAll());
                //     $jumlahYa = count($this->barang->where($k3, $v3)->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                //     $jumlahTidak = count($this->barang->where($k3, $v3)->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                //     $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

                //     $attrNode3 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);


                //     // Tandai
                //     $attrAllNode = $this->getAttrAllNode3($this->attribute, $jumlahBarang, $entrophy, $k1, $v1, $k2, $v2, $k3, $v3);

                //     // dd($attrNode3, $attrAllNode, $k3);
                    
                //     array_push($wadah, $attrAllNode);
                //     // dd($wadah);

                // }

            }
        }
        return $wadah;
    }
    public function node4_attr () {
        $this->pulihkan();

        $jumlahTotal = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());

        $entrophyTotal = $this->getEntrophy($jumlahTotal, $jumlahYa, $jumlahTidak);

        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($this->attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadah_data_smt
            ];
            array_push($wadahGain, $data);

        }

        $attrNode1 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophyTotal);

        $this->pulihkan();
        $nama = array_keys($attrNode1);
        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node1 = $index;
        
        // array_splice($this->attribute, $index, 1);
        unset($this->attribute[$this->node1]);

        // dd($this->attribute);


        $wadah = [];

        $k1 = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        foreach ($values as $v1) {
            $jumlahBarang = count($this->barang->where($k1, $v1)->findAll());
            $jumlahYa = count($this->barang->where($k1, $v1)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
            // $attrAllNode = $this->getAttrAllNode($this->attribute, $jumlahBarang, $entrophy);
            // array_push($wadah, $attrAllNode);

            // Pembatas
            $this->pulihkan();
            $nama = array_keys($attrNode2);

            // dd($nama);
            $namas = [];
            foreach ($this->attribute as $attr) {
                $n = array_keys($attr)[0];
                array_push($namas, $n);
            }
            $index = array_search($nama[0], $namas);
            $this->node2 = $index;
            unset($this->attribute[$this->node1], $this->attribute[$this->node2]);
            $k2 = array_keys($attrNode1)[0];
            $values = array_values($attrNode1)[0];
            foreach ($values as $v2) {
                $jumlahBarang = count($this->barang->where($k2, $v2)->where($k1, $v1)->findAll());
                $jumlahYa = count($this->barang->where($k2, $v2)->where($k1, $v1)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($k2, $v2)->where($k1, $v1)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

                $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
                $this->pulihkan();

                $nama = array_keys($attrNode2);
                // dd($nama);
                $namas = [];
                foreach ($this->attribute as $attr) {
                    $n = array_keys($attr)[0];
                    array_push($namas, $n);
                }
                $index = array_search($nama[0], $namas);
                // dd($nama, $namas, $index);
                $this->node3 = $index;
                unset($this->attribute[$this->node1], $this->attribute[$this->node2], $this->attribute[$this->node3]);
                $k3 = array_keys($attrNode1)[0];
                $values = array_values($attrNode1)[0];
                foreach ($values as $v3) {
                    $jumlahBarang = count($this->barang->where($k3, $v3)->where($k1, $v1)->where($k2, $v2)->findAll());
                    $jumlahYa = count($this->barang->where($k3, $v3)->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                    $jumlahTidak = count($this->barang->where($k3, $v3)->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                    $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

                    $attrNode3 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
                    $this->pulihkan();
                    $nama = array_keys($attrNode1)[0];
                    $namas = [];
                    foreach ($this->attribute as $attr) {
                        $n = array_keys($attr)[0];
                        array_push($namas, $n);
                    }
                    $index = array_search($nama[0], $namas);
                    // dd($nama, $namas, $index);
                    $this->node4 = $index;

                    // dd($nama, $namas, $index);


                    // dd($this->attribute);

                    // array_splice($this->attribute, $this->node1, 1);
                    // array_splice($this->attribute, $this->node2, 1);
                    // array_splice($this->attribute, $this->node3, 1);

                    unset($this->attribute[$this->node1], $this->attribute[$this->node2], $this->attribute[$this->node3]);
                    
                    // array_splice($this->attribute, $this->node4, 1);

                    // dd($this->attribute);


                    $key = array_keys($attrNode1)[0];
                    $values = array_values($attrNode1)[0];
                    foreach ($values as $value) {
                        $jumlahBarang = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($key, $value)->findAll());
                        $jumlahYa = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($key, $value)->where('minat', 'naik')->findAll());
                        $jumlahTidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($key, $value)->where('minat', 'turun')->findAll());
                        $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

                        $attrAllNode = $this->getAttrAllNode($this->attribute, $jumlahBarang, $entrophy);




                        $data = [
                            "nama" => $key,
                            "value" => $value,
                            "data" => [
                                "ya" => $jumlahYa,
                                "tidak" => $jumlahTidak
                            ]
                        ];
                        array_push($wadah, $data);
                        // dd($wadah);

                    }
                    // $this->pulihkan();


                }

            }
        }
        return $wadah;
    }
    
















    public function node1() {
        $this->pulihkan();

        $jumlahBarang = count($this->barang->findAll());
        $jumlahYa = count($this->barang->where('minat', 'naik')->findAll());
        $jumlahTidak = count($this->barang->where('minat', 'turun')->findAll());

        $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

        $attrNode1 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);


        $node = $this->getAttrNode2($attrNode1);
        
        return $node;

    }

    public function getAttrAllNode($attribute, $jumlahTotal, $entrophyTotal) {
        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }


        $this->total = $wadahGain;


        $keys = [];
        $values = [];
        foreach ($wadahGain as $gain) {
            $key = $gain['nama'];
            $value = $gain['gain'];
            array_push($keys, $key);
            array_push($values, $value);
        }

        // dd($wadahGain);

        
        



        return $wadahGain;
    }


    public function getAttrAllNode1($attribute, $jumlahTotal, $entrophyTotal, $k1, $v1) {
        // dd($attribute);
        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->where($k1, $v1)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where($k1, $v1)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where($k1, $v1)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }

        return $wadahGain;
    }
    public function getAttrAllNode1Max($attribute, $jumlahTotal, $entrophyTotal, $k1, $v1) {
        // dd($attribute);
        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->where($k1, $v1)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where($k1, $v1)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where($k1, $v1)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }


        $keys = [];
        $values = [];
        foreach ($wadahGain as $gain) {
            $key = $gain['nama'];
            $value = $gain['gain'];
            array_push($keys, $key);
            array_push($values, $value);
        }


        
        $gainMax = max($values);
        $index = array_search($gainMax, $values);
        $key = $keys[$index];

        $labels = [];
        $barang = $this->barang->select($key)->findAll();
        foreach($barang as $b) {
            array_push($labels, $b[$key]);
        }

        $label = array_unique($labels);
        
        $data = [
            $key => $label
        ];

        // var_dump($wadahGain);
        // echo '<br><br>';
        return $data;

    }
    public function getAttrAllNode2($attribute, $jumlahTotal, $entrophyTotal, $k1, $v1, $k2, $v2) {

        $wadahGain = [];
        // dd($k1, $v1, $k2, $v2);
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->where($k1, $v1)->where($k2, $v2)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            // dd($wadah_data_smt);
            
            $gain = 0;
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }
        return $wadahGain;
    }
    public function getAttrAllNode3($attribute, $jumlahTotal, $entrophyTotal, $k1, $v1, $k2, $v2, $k3, $v3) {
        $wadahGain = [];
        // dd($k1, $v1, $k2, $v2);
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
            if(count($wadahEntrophy) == 2) {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            } else {
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah'], $wadahEntrophy[2]['entrophy'], $wadahEntrophy[2]['jumlah']);
            }
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadahEntrophy
            ];
            array_push($wadahGain, $data);

        }


        $this->total = $wadahGain;


        $keys = [];
        $values = [];
        foreach ($wadahGain as $gain) {
            $key = $gain['nama'];
            $value = $gain['gain'];
            array_push($keys, $key);
            array_push($values, $value);
        }

        // dd($wadahGain);

        
        



        return $wadahGain;
    }


    public function getAttrNode1($attribute, $jumlahTotal, $entrophyTotal) {
        $wadahGain = [];
        $wadah_data_smt = [];
        foreach ($attribute as $attr) {
            $attrName = array_keys($attr)[0];
            $wadahEntrophy = [];
            foreach ($attr[$attrName] as $value) {
                $jumlahBarang = count($this->barang->where($attrName, $value)->findAll());
                $jumlahYa = count($this->barang->where($attrName, $value)->where('minat', 'naik')->findAll());
                $jumlahTidak = count($this->barang->where($attrName, $value)->where('minat', 'turun')->findAll());
                $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);
                $data = [
                    "key" => $attrName,
                    "value" => $value,
                    "jumlah" => $jumlahBarang,
                    "jumlahYa" => $jumlahYa,
                    "jumlahTidak" => $jumlahTidak,
                    "entrophy" => $entrophy
                ];
                array_push($wadahEntrophy, $data);
            }

            array_push($wadah_data_smt, $wadahEntrophy);
            
            $gain = 0;
                $gain = $this->getGain($entrophyTotal, $jumlahTotal, $wadahEntrophy[0]['entrophy'], $wadahEntrophy[0]['jumlah'], $wadahEntrophy[1]['entrophy'], $wadahEntrophy[1]['jumlah']);
            
            $data = [
                "nama" => $wadahEntrophy[0]['key'],
                "gain" => $gain,
                "detail" => $wadah_data_smt
            ];

            array_push($wadahGain, $data);

        }

        $this->total = $wadahGain;


        $keys = [];
        $values = [];
        foreach ($wadahGain as $gain) {
            $key = $gain['nama'];
            $value = $gain['gain'];
            array_push($keys, $key);
            array_push($values, $value);
        }


        
        $gainMax = max($values);
        $index = array_search($gainMax, $values);
        $key = $keys[$index];

        $labels = [];
        $barang = $this->barang->select($key)->findAll();
        foreach($barang as $b) {
            array_push($labels, $b[$key]);
        }

        $label = array_unique($labels);
        
        $data = [
            $key => $label
        ];

        // var_dump($wadahGain);
        // echo '<br><br>';
        return $data;


    }

    public function getAttrNode2($attrNode1) {
        $this->pulihkan();
        $nama = array_keys($attrNode1);


        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node1 = $index;
        
        // array_splice($this->attribute, $index, 1);
        unset($this->attribute[$this->node1]);

        // dd($this->attribute);



        $key = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        $wadah = [];
        foreach ($values as $value) {
            $jumlahBarang = count($this->barang->where($key, $value)->findAll());
            $jumlahYa = count($this->barang->where($key, $value)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($key, $value)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
            $node = $this->getAttrNode3($attrNode2, $key, $value);

            // dd($node);

            $data = [
                "nama" => $key,
                "value" => $value,
                "data" => $node
            ];
            array_push($wadah, $data);
            // dd($wadah);

        }

        return $wadah;
        
    }

    public function getAttrNode3($attrNode1, $k1, $v1) {
        $this->pulihkan();
        $nama = array_keys($attrNode1);

        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($index);
        $this->node2 = $index;
        // dd($nama, $namas, $index);


        // dd($this->attribute);
        unset($this->attribute[$this->node1], $this->attribute[$this->node2]);
        // array_splice($this->attribute, $this->node1, 1);
        // array_splice($this->attribute, $this->node2, 1);

        // dd($this->attribute);


        $key = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        $wadah = [];
        foreach ($values as $value) {
            $jumlahBarang = count($this->barang->where($key, $value)->where($k1, $v1)->findAll());
            $jumlahYa = count($this->barang->where($key, $value)->where($k1, $v1)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($key, $value)->where($k1, $v1)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attrNode2 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
            $node = $this->getAttrNode4($attrNode2, $k1, $v1, $key, $value);

            // dd($node);

            $data = [
                "nama" => $key,
                "value" => $value,
                "data" => $node
            ];
            array_push($wadah, $data);
            // dd($wadah);

        }

        return $wadah;
        
    }



    public function getAttrNode4($attrNode1, $k1, $v1, $k2, $v2) {
        $this->pulihkan();
        $nama = array_keys($attrNode1);
        // dd($nama);
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node3 = $index;
        // dd($nama, $namas, $index);


        // array_splice($this->attribute, $this->node1, 1);
        // array_splice($this->attribute, $this->node2, 1);
        // array_splice($this->attribute, $this->node3, 1);
        unset($this->attribute[$this->node1], $this->attribute[$this->node2], $this->attribute[$this->node3]);


        // dd($this->attribute);


        $key = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        $wadah = [];
        foreach ($values as $value) {
            $jumlahBarang = count($this->barang->where($key, $value)->where($k1, $v1)->where($k2, $v2)->findAll());
            $jumlahYa = count($this->barang->where($key, $value)->where($k1, $v1)->where($k2, $v2)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($key, $value)->where($k1, $v1)->where($k2, $v2)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            $attrNode3 = $this->getAttrNode1($this->attribute, $jumlahBarang, $entrophy);
            $node = $this->getAttrNode5($attrNode3, $k1, $v1, $k2, $v2, $key, $value);

            $data = [
                "nama" => $key,
                "value" => $value,
                "data" => $node
            ];
            array_push($wadah, $data);
            // dd($wadah);

        }

        return $wadah;

    }
    public function getAttrNode5($attrNode1, $k1, $v1, $k2, $v2, $k3, $v3) {
        $this->pulihkan();
        $nama = array_keys($attrNode1)[0];
        $namas = [];
        foreach ($this->attribute as $attr) {
            $n = array_keys($attr)[0];
            array_push($namas, $n);
        }
        $index = array_search($nama[0], $namas);
        // dd($nama, $namas, $index);
        $this->node4 = $index;

        // dd($nama, $namas, $index);


        // dd($this->attribute);

        // array_splice($this->attribute, $this->node1, 1);
        // array_splice($this->attribute, $this->node2, 1);
        // array_splice($this->attribute, $this->node3, 1);

        unset($this->attribute[$this->node1], $this->attribute[$this->node2], $this->attribute[$this->node3]);
        
        // array_splice($this->attribute, $this->node4, 1);

        // dd($this->attribute);


        $key = array_keys($attrNode1)[0];
        $values = array_values($attrNode1)[0];
        $wadah = [];
        foreach ($values as $value) {
            $jumlahBarang = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($key, $value)->findAll());
            $jumlahYa = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($key, $value)->where('minat', 'naik')->findAll());
            $jumlahTidak = count($this->barang->where($k1, $v1)->where($k2, $v2)->where($k3, $v3)->where($key, $value)->where('minat', 'turun')->findAll());
            $entrophy = $this->getEntrophy($jumlahBarang, $jumlahYa, $jumlahTidak);

            // $node = $this->metodeKedua->getAttrNode5($attrNode3);

            // $key = array_keys($attrNode3)[0];




            $data = [
                "nama" => $key,
                "value" => $value,
                "data" => [
                    "ya" => $jumlahYa,
                    "tidak" => $jumlahTidak
                ]
            ];
            // dd($data);

            array_push($wadah, $data);
            // dd($wadah);

        }
        // $this->pulihkan();

        return $wadah;

    }

    








    public function getEntrophy ($jumlahBarang, $jumlahYa, $jumlahTidak) {

        if($jumlahYa == 0 || $jumlahTidak == 0) {
            return 0;
        }
        $entrophy = (-$jumlahTidak/$jumlahBarang * log($jumlahTidak/$jumlahBarang, 2)) + (-$jumlahYa/$jumlahBarang * log($jumlahYa/$jumlahBarang, 2));

        return round($entrophy, 3);

    }

    public function getGain ($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2) {       
        $attr1;
        $attr2;
        if($jumlahTotal == 0 || $entrophyAttr1 == 0 || $jumlahAttr1 == 0) {
        $attr1 = 0;
        } else {
            $attr1 = round($jumlahAttr1/$jumlahTotal*$entrophyAttr1, 3);
        }
        if($jumlahTotal == 0 || $entrophyAttr2 == 0 || $jumlahAttr2 == 0) {
        $attr2 = 0;
        } else {
            $attr2 = round($jumlahAttr2/$jumlahTotal*$entrophyAttr2, 3);

        }
        
        // dd($jumlahTotal);
        
        
        // dd(($jumlahAttr1/$jumlahTotal*$entrophyAttr1));
        // dd(($jumlahAttr2/$jumlahTotal*$entrophyAttr2));
        // dd($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2);
        $gain = round($entrophyTotal-($attr1+$attr2), 3);
        
        // dd($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2, $attr1, $attr2, $attr1+$attr2, $gain);

        return round($gain, 3);

    }
    public function getGains ($entrophyTotal, $jumlahTotal, $entrophyAttr1, $jumlahAttr1, $entrophyAttr2, $jumlahAttr2, $entrophyAttr3, $jumlahAttr3) {
        $gain = $entrophyTotal-(($jumlahAttr1/$jumlahTotal*$entrophyAttr1)+($jumlahAttr2/$jumlahTotal*$entrophyAttr2)+($jumlahAttr3/$jumlahTotal*$entrophyAttr3));


        return $gain;

    }
}
