<?php

namespace App\Models;

use CodeIgniter\Model;

class Penjualan extends Model
{
    
    protected $table      = 'penjualan';
    protected $allowedFields  = ['id_barang', 'minggu', 'bulan', 'tahun', 'jumlah'];

    public function penjualanTerakhir($id) {     
        $db = db_connect();
        $tahunTerbesar = $db->query('SELECT MAX(tahun) FROM penjualan WHERE (id_barang=?)', [$id])->getResultArray()[0]['MAX(tahun)'];
        $bulanTerbesar = $db->query('SELECT MAX(bulan) FROM penjualan WHERE id_barang=? AND tahun = ?', [$id, $tahunTerbesar])->getResultArray()[0]['MAX(bulan)'];
        $mingguTerbesar = $db->query('SELECT MAX(minggu) FROM penjualan WHERE id_barang=? AND tahun = ? AND bulan = ?', [$id, $tahunTerbesar, $bulanTerbesar])->getResultArray()[0]['MAX(minggu)'];
        $penjualanTerakhir = $db->query('SELECT jumlah FROM penjualan WHERE id_barang=? AND tahun = ? AND bulan = ? AND minggu = ?', [$id, $tahunTerbesar, $bulanTerbesar, $mingguTerbesar]);

        if($penjualanTerakhir->getResultArray() == null) {
            return 0;
        }     
        


        return $penjualanTerakhir->getResultArray()[0]['jumlah'];
    }

    // public function rekapPenjualan($id) {
    //     $db = db_connect();
    //     $tahunTerbesar = $db->query('SELECT MAX(tahun) FROM penjualan WHERE (id_barang=?)', [$id])->getResultArray()[0]['MAX(tahun)'];
    //     $bulanTerbesar = $db->query('SELECT MAX(bulan) FROM penjualan WHERE id_barang=? AND tahun = ?', [$id, $tahunTerbesar])->getResultArray()[0]['MAX(bulan)'];
    //     $mingguTerbesar = $db->query('SELECT MAX(minggu) FROM penjualan WHERE id_barang=? AND tahun = ? AND bulan = ?', [$id, $tahunTerbesar, $bulanTerbesar])->getResultArray()[0]['MAX(minggu)'];

    //     $bulanTerbesarKedua = 0;
    //     $tahunTerbesarKedua = 0;
    //     $mingguTerbesarKedua = 0;

    //     if($mingguTerbesar == 1) {
    //         $mingguTerbesarKedua = 4;
    //         $bulanTerbesarKedua = $bulanTerbesar - 1;
    //         if($bulanTerbesarKedua == 0) {
    //             $tahunTerbesarKedua = $tahunTerbesar - 1;
    //         } else {
    //             $tahunTerbesarKedua = $tahunTerbesar;
    //         }
    //     } else {
    //         $mingguTerbesarKedua = $mingguTerbesar - 1;
    //         $bulanTerbesarKedua = $bulanTerbesar;
    //         $tahunTerbesarKedua = $tahunTerbesar;
    //     }








    //     $penjualanTerakhirKeduaQuery = $db->query('SELECT jumlah FROM penjualan WHERE id_barang=? AND tahun = ? AND bulan = ? AND minggu = ?', [$id, $tahunTerbesarKedua, $bulanTerbesarKedua, $mingguTerbesarKedua]);

    //     $penjualanTerakhirKedua = 0;
    //     if($penjualanTerakhirKeduaQuery->getResultArray() == null) {
    //         $penjualanTerakhirKedua = 0;
    //     } else {
    //         $penjualanTerakhirKedua = $penjualanTerakhirKeduaQuery->getResultArray()[0]['jumlah'];
    //     }

    //     $penjualanTerakhir = $this->penjualanTerakhir($id);

    //     $rekap = "";
    //     if($penjualanTerakhir > $penjualanTerakhirKedua) {
    //         $rekap = "naik";
    //     } else if ($penjualanTerakhir == $penjualanTerakhirKedua) {
    //         $rekap = "sama";
    //     } else {
    //         $rekap = "turun";
    //     }

    //     $db->query('UPDATE barang SET penjualan = ? WHERE id = ?', [$rekap, $id]);
    // }
        
    
}