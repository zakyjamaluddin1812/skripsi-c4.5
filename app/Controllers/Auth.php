<?php

namespace App\Controllers;

class Auth extends BaseController
{
    protected $user;
    protected $helpers = ['url', 'form'];
    
    
    protected $session;
    public function __construct () {
        $this->session = session();
        $this->user = model('../Models/User');
        
    }

    public function daftar () {
        if($this->session->get('login') == true) {
            return redirect()->to(base_url('/dashboard'));
        }
        $name = $this->request->getPost('name');
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $password_confirm = $this->request->getPost('password-confirm');

        if($password !== $password_confirm) {
            return redirect()->back()->with('password-error', true);
        }

        $user = [
            "name" => $name,
            "email" => $email,
            "password" => $password
        ];

        $this->user->save($user);

        return redirect()->to(base_url('/'))->with('login-berhasil', true);
    }

    public function login() {

        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $user = $this->user->where('email', $email)->where('password', $password)->first();
        if ($user == null) {
            return redirect()->back()->with('gagal', true);
        } else {
            $this->session->set('login', true);
            return redirect()->to(base_url('/dashboard'));
        }
    }

    public function logout() {
        $this->session->remove('login');
        return redirect()->to(base_url('/'));

    }
}