<?php

namespace App\Controllers;

class Decission extends BaseController
{
    protected $barang;
    protected $metode;
    protected $methode;
    protected $decission;
    protected $session;    
    public function __construct() {
        $this->barang = model('../Models/Barang');
        $this->metode = model('../Models/Metode');
        $this->methode = model('../Models/Methode');
        $this->decission = model('../Models/Decission');
        $this->session = session();


    }
    public function index()
    {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }

        $data['page'] = "Decission Tree";

        
        
        $decission = $this->methode->decission_tree()  ;
        $data['decission'] = $decission;
        // dd($decission);

        // dd($data['decission']);



        return view('templates/header', $data)
            . view('decission', $data)
            . view('templates/footer');
    }

    public function detail() 
    {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }

        
        $semua = $this->decission->semua();
        $node1 = $this->decission->node1();
        $node2 = $this->decission->node2();
        $node3 = $this->decission->node3();
        $node4 = $this->decission->node4();

        // dd($semua);




        $data['semua'] = $semua;
        $data['node1'] = $node1;
        $data['node2'] = $node2;
        $data['node3'] = $node3;
        $data['node4'] = $node4;
        // $data['total'] = $this->metode->total();
        // $data['total_attr'] = $this->metode->total_attr();
        // $data['node1_attr'] = $this->metode->node1_attr();
        // $data['node2_attr'] = $this->metode->node2_attr();
        // $data['node3_attr'] = $this->metode->node3_attr();
        // $data['node4_attr'] = $this->metode->node4_attr();

        // dd($data['node4_attr']);
        $data['page'] = "Decission Tree";
        // dd($data['node1_attr']);

        return view('templates/header', $data)
            . view('decission_detail', $data)
            . view('templates/footer');
    }

    public function ramalan()
    {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }

        $data['page'] = "Hasil Ramalan";

        $data['decission'] = $this->metode->node1();



        return view('templates/header', $data)
            . view('ramalan', $data)
            . view('templates/footer');
    }

    public function ramal() {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $model = $this->request->getPost('model');
        $warna = $this->request->getPost('warna');
        $ukuran = $this->request->getPost('ukuran');
        $kualitas_bahan = $this->request->getPost('kualitas_bahan');


        $minat;

        if($model == "" || $warna == "" || $ukuran == "" || $kualitas_bahan == "") {
            $this->session->remove('minat');
            return redirect()->back();
        }

        if($ukuran == "allsize") {
            if ($warna == "banyak") {
                if ($model == "Atasan") {
                    if ($kualitas_bahan == "baik") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                } else {
                    if ($kualitas_bahan == "baik") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                }
            } else {
                if ($model == "Atasan") {
                    if ($kualitas_bahan == "baik") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                } else {
                    if ($kualitas_bahan == "baik") {
                        $minat = "turun";
                    } else {
                        $minat = "turun";
                    }
                }
            }
        } else {
            if ($kualitas_bahan == "baik") {
                if ($model == "Atasan") {
                    if ($warna == "banyak") {
                        $minat = "naik";
                    } else {
                        $minat = "naik";
                    }
                } else {
                    if ($warna == "banyak") {
                        $minat = "turun";
                    } else {
                        $minat = "turun";
                    }
                }
            } else {
                if ($warna == "banyak") {
                    if ($model == "Atasan") {
                        $minat = "turun";
                    } else {
                        $minat = "turun";
                    }
                } else {
                    if ($model == "Atasan") {
                        $minat = "naik";
                    } else {
                        $minat = "turun";
                    }
                }
            }
        }

        return redirect()->back()->with("minat", $minat);
        
    }

    
}
