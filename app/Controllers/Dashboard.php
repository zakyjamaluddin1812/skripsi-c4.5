<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    protected $barang;
    protected $metode;
    protected $session;
    public function __construct () {
        $this->session = session();
        $this->barang = model('../Models/Barang');
        $this->metode = model('../Models/Metode');


    }
    public function index() {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $metode = $this->metode->node1();
        $minat_naik = count($this->barang->select('minat')->where('minat', 'naik')->findAll());
        $minat_turun = count($this->barang->select('minat')->where('minat', 'turun')->findAll());

        $minat = "naik";
        if($minat_naik < $minat_turun) {
            $minat = "turun";
        }

        $data['minat'] = $minat;
        


        $data['barang'] = $this->barang->findAll();
        $data['page'] = "Dashboard";
        
        return view('templates/header', $data)
            . view('dashboard', $data)
            . view('templates/footer');
    }
}