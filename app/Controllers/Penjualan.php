<?php

namespace App\Controllers;

class Penjualan extends BaseController
{
    protected $barang;
    protected $penjualan;
    protected $decission;
    protected $session;
    public function __construct () {
        $this->session = session();
        $this->barang = model('../Models/Barang');
        $this->penjualan = model('../Models/Penjualan');
        $this->decission = model('../Models/Decission');
    }
    public function index() {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $data['page'] = "Data Penjualan";


        $data['barang'] = $this->barang->findAll();
        $penjualan = [];
        foreach ($data['barang'] as $b) {
            $penjualanTerakhir = $this->penjualan->penjualanTerakhir($b['id']);
            
            $terakhir = [
                'id' => $b['id'],
                'jumlah' => $penjualanTerakhir
            ];

            array_push($penjualan, $terakhir);
        }
        $data['penjualanTerakhir'] = $penjualan;


        // foreach($data['barang'] as $d) {
        //     $this->penjualan->rekapPenjualan($d['id']);
            
        // }


        






        return view('templates/header', $data)
            . view('penjualan', $data)
            . view('templates/footer');
    }

    public function detailBarang($id) {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $data['page'] = "Data Penjualan";

        $barang = $this->barang->where('id', $id)->first();
        $data['barang'] = $barang;
        return view('templates/header', $data)
            . view('detail_barang', $data)
            . view('templates/footer');
    }

    public function detailPenjualan($id) {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $data['page'] = "Data Penjualan";

        $penjualan = $this->penjualan->where('id_barang', $id)->find();
        $data['penjualan'] = $penjualan;
        return view('templates/header', $data)
            . view('detail_penjualan', $data)
            . view('templates/footer');
    }

    public function tambah_barang () {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $nama = $this->request->getPost('nama');
        $model = $this->request->getPost('model');
        $warna = $this->request->getPost('warna');
        $ukuran = $this->request->getPost('ukuran');
        $kualitas_bahan = $this->request->getPost('kualitas_bahan');

        $minat = $this->decission->decission($model, $warna, $ukuran, $kualitas_bahan);

        $barang = [
            "nama" => $nama,
            "model" => $model,
            "warna" => $warna,
            "ukuran" => $ukuran,
            "kualitas_bahan" => $kualitas_bahan,
            "minat" => $minat
        ];

        $this->barang->insert($barang);
        return redirect()->to(base_url('/tables'));
    }

    public function hapus_barang ($id) {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $this->barang->delete($id);
        return redirect()->to(base_url('/tables'));
    }

    public function tambah_penjualan () {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $id_barang = $this->request->getPost('id_barang');
        $minggu = $this->request->getPost('minggu');
        $bulan = $this->request->getPost('bulan');
        $tahun = $this->request->getPost('tahun');
        $jumlah = $this->request->getPost('jumlah');

        $penjualan = [
            "id_barang" => $id_barang,
            "minggu" => $minggu,
            "bulan" => $bulan,
            "tahun" => $tahun,
            "jumlah" => $jumlah,
        ];

        $this->penjualan->insert($penjualan);
        return redirect()->to(base_url('/detail-penjualan/'.$id_barang));
    }

    public function hapus_penjualan($id, $id_barang)
    {
        if($this->session->get('login') !== true) {
            return redirect()->to(base_url('/'));
        }
        $this->penjualan->delete($id);
        return redirect()->to(base_url('/detail-penjualan/'.$id_barang));
        
    }
}