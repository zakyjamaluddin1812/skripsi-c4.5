<div class="container-fluid py-4">
<a href="/decission-detail" class="btn btn-light">Detail</a>

  <div class="row">
  <div class="row">
      
      <div class="col-12">
        <div class="card  mb-4">
          <div class="card-header pb-0">
            <h6>Decission Tree</h6>
          </div>
          <div class="card-body overflow-scroll">
            
          <table class="table text-center">
              <thead>
                  <tr>
                  <th scope="col">#</th>
                  <th scope="col">node1</th>
                  <th scope="col">value1</th>
                  <th scope="col">node2</th>
                  <th scope="col">value2</th>
                  <th scope="col">node3</th>
                  <th scope="col">value3</th>
                  <th scope="col">node4</th>
                  <th scope="col">value4</th>
                  <th scope="col">keputusan</th>
                  </tr>
              </thead>
              <tbody>
                  <?php $i = 1;?>
                  <?php foreach($decission as $n) :?>
                  <tr>
                              <th scope="row"><?=$i?></th>
                              <td><?=$n['node1']?></td>
                              <td><?=$n['value1']?></td>
                              <td><?=$n['node2']?></td>
                              <td><?=$n['value2']?></td>
                              <td><?=$n['node3']?></td>
                              <td><?=$n['value3']?></td>
                              <td><?=$n['node4']?></td>
                              <td><?=$n['value4']?></td>
                              <td><?=$n['keputusan']?></td>
                          
                      </tr>
                      <?php $i++ ?>
                      <?php endforeach ?>

              </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      