<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Detail Barang</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Ukuran</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Model</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kualitas bahan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Warna</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= $barang['nama']?></h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <?= $barang['ukuran']?>
                      </td>
                      <td class="align-middle text-center text-sm">
                        <?= $barang['model']?>
                      </td>
                      <td class="align-middle text-center text-sm">
                        <?= $barang['kualitas_bahan']?>
                      </td>
                      <td class="align-middle text-center text-sm">
                        <?= $barang['warna']?>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <a href="/hapus-barang/<?= $barang['id']?>"><button class="btn btn-danger mx-3">Hapus Barang</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      