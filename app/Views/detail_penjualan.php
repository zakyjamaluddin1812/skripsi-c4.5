<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Detail Penjualan</h6>
            </div>
            <button class="btn btn-success mx-3" style="width:200px;" data-bs-toggle="modal" data-bs-target="#exampleModal">Tambah Penjualan</button>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Minggu</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Bulan</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tahun</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Jumlah</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if($penjualan == null) :?>
                      <tr>
                        <td colspan="4" class="text-center">Kosong</td>
                      </tr>
                    <?php endif?>
                    
                    <?php for($i = 0; $i<count($penjualan); $i++) :?>
                    <tr class="<?= $penjualan[$i]['bulan']%2 == 0 ? "bg-light": ""?>">
                      <td>
                        
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= $penjualan[$i]['minggu']?></h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= $penjualan[$i]['bulan']?></h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= $penjualan[$i]['tahun']?></h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= $penjualan[$i]['jumlah']?></h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="d-flex justify-content-center px-3 py-1">
                          
                          <div class="d-flex flex-column justify-content-center ">
                            <a href="/hapus-penjualan/<?= $penjualan[$i]['id']?>/<?= $penjualan[$i]['id_barang']?>">    
                              <span class="badge rounded-pill px-3 text-bg-danger">Hapus</span>
                            </a>
                            
                          </div>
                        </div>
                      </td>
                    </tr>
                    <?php endfor?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="/tambah-penjualan" method="post">
          <input type="hidden" name="id_barang" value="<?= $penjualan[0]['id_barang']?>">
      <select required class="mb-3 form-select" name="minggu" aria-label="Default select example">
        <option selected value="" >Minggu</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        
      </select>
      <select required class="mb-3 form-select" name="bulan" aria-label="Default select example">
        <option selected value="" >Bulan</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
      </select>
      <div class="mb-3">
        <input required type="number" class="form-control" id="exampleFormControlInput1" placeholder="Tahun" name="tahun">
      </div>
      <div class="mb-3">
        <input required type="number" class="form-control" id="exampleFormControlInput1" placeholder="Jumlah" name="jumlah">
      </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>

        
      </div>
    </div>
  </div>
</div>