<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Tabel Barang</h6>
            </div>
            <button data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-success mx-3" style="width: 200px;">Tambah Barang Baru</button>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Barang</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Penjualan Minggu Terakhir</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php for($i = 0; $i<count($barang); $i++) :?>
                    <tr>
                      <td>
                        <div class="d-flex px-3 py-1">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= $barang[$i]['nama']?></h6>
                            <p class="text-xs text-secondary mb-0"><?= $barang[$i]['model']?>/<?= $barang[$i]['ukuran']?>/<?= $barang[$i]['kualitas_bahan']?></p>
                          </div>
                        </div>
                      </td>
                      <td>
                        <?= $penjualanTerakhir[$i]['jumlah']?>
                      </td>
                      <td class="align-middle text-center text-sm">
                        <a href="/detail-penjualan/<?= $barang[$i]['id']?>"><button class="badge  bg-primary">detail penjualan</button></a>
                        <a href="/detail-barang/<?= $barang[$i]['id']?>"><button class="badge  bg-danger">detail barang</button></a>
                      </td>
                    </tr>
                    <?php endfor?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Barang Baru</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <form action="/tambah-barang" method="post">
          <div class="modal-body">
          <div class="mb-3">
            <label for="nama" class="form-label">nama</label>
            <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama" require>
          </div>
          <div class="mb-3">
            <label for="ukuran" class="form-label">ukuran</label>
            <select name="ukuran" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Ukuran</option>
                    <option value="allsize">allsize</option>
                    <option value="oversize">oversize</option>
                    
                </select>
          </div>
          <div class="mb-3">
            <label for="model" class="form-label">model</label>
            <select name="model" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Model</option>
                    <option value="Atasan">Atasan</option>
                    <option value="Bawahan">Bawahan</option>
                    
                </select>
          </div>
          <div class="mb-3">
            <label for="kualitas_bahan" class="form-label">kualitas_bahan</label>
            <select name="kualitas_bahan" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Kualitas Bahan</option>
                    <option value="baik">baik</option>
                    <option value="premium">premium</option>
                    
                </select>
          </div>
          <div class="mb-3">
            <label for="warna" class="form-label">warna</label>
            <select name="warna" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Warna</option>
                    <option value="Banyak">Banyak</option>
                    <option value="Sedikit">Sedikit</option>
                    
                </select>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
          </form>
        </div>
      </div>
    </div>
      