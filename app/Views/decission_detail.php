<div class="container-fluid py-4">

      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Pohon Keputusan</h6>
            </div>
            <div class="card-body">
              
            <table class="table text-center">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Total</th>
                    <th scope="col">Ya</th>
                    <th scope="col">Tidak</th>
                    <th scope="col">Entrophy</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td><?=$semua['total']?></td>
                    <td><?=$semua['ya']?></td>
                    <td><?=$semua['tidak']?></td>
                    <td><?=$semua['entrophy']?></td>
                    </tr>
                </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>



      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Menentukan Node 1</h6>
            </div>
            <div class="card-body">
              
            <table class="table text-center">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Ya</th>
                    <th scope="col">Tidak</th>
                    <th scope="col">Entrophy</th>
                    <th scope="col">Gain</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; $j=1;?>
                    <?php foreach($node1 as $n) :?>
                    <tr class="<?= $n['max'] == true? 'bg-light' : ' '?>">
                            <?php if($i%2== 1) :?>
                                
                                <th scope="row" rowspan="2"><?=$j?></th>
                                <td rowspan="2"><?=$n['key']?></td>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <td rowspan="2"><?=$n['gain']?></td>
                            <?php else :?>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <?php $j++ ?>
                            <?php endif ?>
                        </tr>
                        <?php $i++ ?>
                        <?php endforeach ?>

                </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>





      <?php $x = 1;?>
      <?php foreach($node2 as $node) :?>
      <div class="row">
      
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Menentukan Node 2.<?= $x?></h6>
            </div>
            <div class="card-body">
              
            <table class="table text-center">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Ya</th>
                    <th scope="col">Tidak</th>
                    <th scope="col">Entrophy</th>
                    <th scope="col">Gain</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; $j=1;?>
                    <?php foreach($node as $n) :?>
                    <tr class="<?= $n['max'] == true? 'bg-light' : ' '?>">
                            <?php if($i%2== 1) :?>
                                
                                <th scope="row" rowspan="2"><?=$j?></th>
                                <td rowspan="2"><?=$n['key']?></td>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <td rowspan="2"><?=$n['gain']?></td>
                            <?php else :?>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <?php $j++ ?>
                            <?php endif ?>
                        </tr>
                        <?php $i++ ?>
                        <?php endforeach ?>

                </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <?php $x++?>
      <?php endforeach?>




      <?php $x = 1;?>
      <?php foreach($node3 as $node) :?>
      <div class="row">
      
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Menentukan Node 3.<?= $x?></h6>
            </div>
            <div class="card-body">
              
            <table class="table text-center">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Ya</th>
                    <th scope="col">Tidak</th>
                    <th scope="col">Entrophy</th>
                    <th scope="col">Gain</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; $j=1;?>
                    <?php foreach($node as $n) :?>
                    <tr class="<?= $n['max'] == true? 'bg-light' : ' '?>">

                            <?php if($i%2== 1) :?>
                                
                                <th scope="row" rowspan="2"><?=$j?></th>
                                <td rowspan="2"><?=$n['key']?></td>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <td rowspan="2"><?=$n['gain']?></td>
                            <?php else :?>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <?php $j++ ?>
                            <?php endif ?>
                        </tr>
                        <?php $i++ ?>
                        <?php endforeach ?>

                </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <?php $x++?>
      <?php endforeach?>



      <?php $x = 1;?>
      <?php foreach($node4 as $node) :?>
      <div class="row">
      
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Menentukan Node 4.<?= $x?></h6>
            </div>
            <div class="card-body">
              
            <table class="table text-center">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Ya</th>
                    <th scope="col">Tidak</th>
                    <th scope="col">Entrophy</th>
                    <th scope="col">Minat</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; $j=1;?>
                    <?php foreach($node as $n) :?>
                    <tr>

                            <?php if($i%2== 1) :?>
                                
                                <th scope="row" rowspan="2"><?=$j?></th>
                                <td rowspan="2"><?=$n['key']?></td>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <td><?=$n['minat'] == null ? "tidak ada data" : ($n['minat'] == 'naik' ? 'naik' : 'turun')?></td>
                            <?php else :?>
                                <td><?=$n['value']?></td>
                                <td><?=$n['total']?></td>
                                <td><?=$n['ya']?></td>
                                <td><?=$n['tidak']?></td>
                                <td><?=$n['entrophy']?></td>
                                <td><?=$n['minat'] == null ? "tidak ada data" : ($n['minat'] == 'naik' ? 'naik' : 'turun')?></td>
                                

                                <?php $j++ ?>
                            <?php endif ?>
                        </tr>
                        <?php $i++ ?>
                        <?php endforeach ?>

                </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <?php $x++?>
      <?php endforeach?>
    </div>
      