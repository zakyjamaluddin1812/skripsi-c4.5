<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Pohon Keputusan</h6>
            </div>
            <div class="card-body">
              <form action="/ramal" method="post">
              <!-- <div class="table-responsive p-0"> -->
                <!-- <h5>Pohon Keputusan</h5> -->
                <select name="model" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Model</option>
                    <option value="Atasan">Atasan</option>
                    <option value="Bawahan">Bawahan</option>
                    
                </select>
                <select name="warna" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Warna</option>
                    <option value="banyak">Banyak</option>
                    <option value="sedikit">Sedikit</option>
                    
                </select>
                <select name="ukuran" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Ukuran</option>
                    <option value="allsize">allsize</option>
                    <option value="oversize">oversize</option>
                    
                </select>
                <select name="kualitas_bahan" class="mb-3 form-select" aria-label="Default select example" required>
                    <option selected value="">Kualitas Bahan</option>
                    <option value="baik">baik</option>
                    <option value="premium">premium</option>
                    
                </select>

                <button type="submit" class="btn btn-success">Lihat peramalan</button>
                <?php if(isset($minat)) :?>
                <button  class="btn btn-danger"><?= $minat ?></button>
                <?php endif?>
                </form>
            </div>
            
            
          </div>
          
          <?php 
          $session = session();
          if($session->getFlashdata('minat') != null) :?>
          <div class="alert alert-<?= $session->getFlashdata('minat') =="naik"?"success":"danger"?>" role="alert">
            <h5 style="text-style:bold;" class="text-center text-light"><?= $session->getFlashdata('minat') ?></h5>
          </div>
          <?php endif?>
        </div>
      </div>
      